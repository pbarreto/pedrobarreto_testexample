﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

	private string levelName;
	AsyncOperation async;

	public static LoadScene instance;

	private string lastScene;

    public bool canLoad;

	void Awake () {
		if (instance == null) {
			instance = this;
		}
		else Destroy(this);

		DontDestroyOnLoad(gameObject);
	}

	public void StartLoadingAdditive(string name) {
		levelName = name;
		lastScene = SceneManager.GetActiveScene().name;
		StartCoroutine("LoadAdditive");
	}

	public void StartLoading(string name,bool avoidRepetition = false) {
        if (avoidRepetition) {
			if (name == levelName)
				return;
        }

		levelName = name;
		lastScene = SceneManager.GetActiveScene().name;
		StartCoroutine("Load");
	}

	public void Reload() {
		levelName = SceneManager.GetActiveScene().name;
		StartCoroutine("Load");
	}

	IEnumerator Load() {
		async = SceneManager.LoadSceneAsync(levelName);
		async.allowSceneActivation = false;
		yield return async;
	}

	IEnumerator LoadAdditive(){
		async = SceneManager.LoadSceneAsync(levelName,LoadSceneMode.Additive);
		async.allowSceneActivation = false;

		while (!async.isDone)
		{
			if (async.progress >= 0.9f){
			    if (canLoad)
					async.allowSceneActivation = true;
			}

			yield return null;
		}
	}

	public void DestroyLoading(string name) {
		try {
			SceneManager.UnloadSceneAsync(name);
		}
        catch {
			Debug.Log("Cant destroy the scene");
        }
	}

	public void ActivateScene() {
		async.allowSceneActivation = true;
	}

	public float LoadProgress(){
		return async.progress;
	}

	public bool CheckIfSceneReady(){
		return async.allowSceneActivation;
	}
}
