﻿using UnityEngine;
using System.Collections;
using System;

public class Timer {
	private float time;
	private float interval;

	public void BeginTimer(float interval){
		time = Time.time;
		this.interval = interval;
	}

	public bool IsTime(){
		return Time.time >= time + interval;
	}

    public float Progression() {
        return  Mathf.Clamp(-time + Time.time,0, interval);
    }

    public string GetTime() {
        float elapsed = time + interval - Time.time;
        string result = string.Empty;

        int d = (int)(elapsed * 100.0f);

        int minutes = 0;
        int seconds = 0;
        int hundredths = 0;

        if (elapsed > 0)
        {
            d = (int)(elapsed * 100.0f);
            minutes = d / (60 * 100);
            seconds = (d % (60 * 100)) / 100;
            hundredths = d % 100;

        }
        return String.Format("{0:00}:{1:00}.{2:00}", minutes, seconds, hundredths);
    }
}
