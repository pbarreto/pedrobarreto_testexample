﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

    public static InputHandler instance;

    #region variables
    private float verticalAxis;
    private float horizontalAxis;
    private bool clickConfirmButton;
    private bool clickCancelButton;
    private bool clickJumpButton;
    private bool holdJumpButton;
    private bool releaseJumpButton;
    private bool clickExtraButton1;
    private bool holdExtraButton1;
    private bool releaseExtraButton1;
    private bool clickLButton;
    private bool clickRButton;
    private bool esc;
    private bool enter;

    Timer clickTimer;
    float clickTime = 0.14f;

    #endregion

    #region properties
    public float VerticalAxis{
        get
        {
            return verticalAxis;
        }
    }

    public float HorizontalAxis{
        get
        {
            return horizontalAxis;
        }
    }

    public bool ClickConfirmButton{
        get
        {
            return clickConfirmButton;
        }
    }

    public bool ClickCancelButton{
        get
        {
            return clickCancelButton;
        }
    }

    public bool ClickJumpButton
    {
        get
        {
            return clickJumpButton;
        }
    }

    public bool ClickExtraButton1
    {
        get
        {
            return clickExtraButton1;
        }
    }

    public bool ClickLButton {
        get {
            return clickLButton;
        }
    }

    public bool ClickRButton {
        get {
            return clickRButton;
        }
    }

    public bool HoldJumpButton {
        get {
            return holdJumpButton;
        }
    }

    public bool ReleaseJumpButton {
        get {
            return releaseJumpButton;
        }
    }

    public bool HoldExtraButton1 {
        get {
            return holdExtraButton1;
        }
    }

    public bool ReleaseExtraButton1 {
        get {
            return releaseExtraButton1;
        }
    }

    public bool Esc { get => esc;}
    public bool Enter { get => enter;}

    #endregion

    void Awake() {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        clickTimer = new Timer();
    }

    void Update(){
        GetInput();
    }

	void GetInput(){
        verticalAxis = Input.GetAxisRaw ("Vertical");
	    horizontalAxis = Input.GetAxisRaw ("Horizontal");
        clickConfirmButton = Input.GetButtonDown ("Fire1");
        clickCancelButton = Input.GetButtonDown ("Fire2");
        clickJumpButton = Input.GetButtonDown("Jump");
        holdJumpButton = Input.GetButton("Jump");
        holdJumpButton = HoldChecker(clickJumpButton, holdJumpButton);
        releaseJumpButton = Input.GetButtonUp("Jump");
        clickExtraButton1 = Input.GetButtonDown("Fire3");
        holdExtraButton1 = Input.GetButton("Fire3");
        releaseExtraButton1 = Input.GetButtonUp("Fire3");
        clickLButton = Input.GetKeyDown(KeyCode.Q);
        clickRButton = Input.GetKeyDown(KeyCode.E);
        esc = Input.GetKeyDown(KeyCode.Escape);
        enter = Input.GetKeyDown(KeyCode.KeypadEnter);
    }


    bool HoldChecker(bool buttonClick,bool ButtonHold) {
        if (buttonClick) {
            clickTimer.BeginTimer(clickTime);
        }
        else if (ButtonHold && clickTimer.IsTime()) {
            return true;
        }
        
        return false;
    }
}
