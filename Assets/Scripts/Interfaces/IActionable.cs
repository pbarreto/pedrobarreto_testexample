﻿public interface IActionable{
	void Play();
	void Deactivate();
	void Activate();
}
