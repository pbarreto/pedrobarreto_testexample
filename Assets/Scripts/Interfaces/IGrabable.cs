﻿using UnityEngine;
public interface IGrabable
{
    void Grab(Transform hand);
    void Release();
}
