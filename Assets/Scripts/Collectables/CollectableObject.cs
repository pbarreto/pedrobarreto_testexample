﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableObject : MonoBehaviour
{
    [SerializeField]
    Collectable collectable;

    UIManager ui;
    MeshRenderer mesh;

    AudioManager am;

    private void Start() {
        ui = UIManager.instance;
        mesh = GetComponent<MeshRenderer>();
        mesh.material = collectable.Material;
        am = AudioManager.instance;
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player") {
            ui.AddScore(collectable.Points);
            am.PlayEffect(ui.transform,"item");
            Destroy(transform.parent.gameObject);
        }
    }
}
