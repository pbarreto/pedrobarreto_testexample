﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Collectable", menuName = "ScriptableObjects/Collectable", order = 1)]
public class Collectable : ScriptableObject
{
    [SerializeField]
    string name;
    [SerializeField]
    int points;
    [SerializeField]
    Material material;

    public string Name { get => name; }
    public int Points { get => points;}
    public Material Material { get => material;}
}
