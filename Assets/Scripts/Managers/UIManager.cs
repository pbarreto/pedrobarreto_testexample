﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
public class UIManager : MonoBehaviour {

	[Header("Grab")]
	[SerializeField]
	GameObject grabIn;
	[SerializeField]
	GameObject grabOut;
	[Header("Pause")]
	[SerializeField]
	GameObject pause;

	[SerializeField]
	TextMeshProUGUI level;
	[SerializeField]
	GameObject mainUI;

	InputHandler input;
	public static UIManager instance;
	LoadScene load;
	int maxPoints;

	[SerializeField]
	TextMeshProUGUI score;

	public int MaxPoints
	{ 
		get{
			return	maxPoints;
		}
		set{
			maxPoints = value;
			if (maxPoints < 0)
				maxPoints = 0;
			else if (maxPoints > 9999)
				maxPoints = 9999;
		}
	}

    void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

    private void Start() {
		SceneManager.sceneLoaded += OnSceneLoaded;
		input = InputHandler.instance;
		load = LoadScene.instance;
		ClickResume();
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		string name = level.text = CurrentLevelUpdater.instance.LoadCurrentLevel();
		level.text = name;
		if (name != "MainMenu")
			ClickResume();
	}

	void Grab(bool inValue, bool outValue) {
		grabIn.SetActive(inValue);
		grabOut.SetActive(outValue);
	}

    private void Update() {
		if (input.Esc || input.Enter)
			if (Time.timeScale == 1)
				Pause();
			else ClickResume();
    }

    public void HideGrab() {
		Grab(false, false);
	}

	public void ShowGrab() {
		Grab(true,false);
	}
	public void ShowRelease() {
		Grab(false,true);
	}

	public void Pause() {
		mainUI.SetActive(false);
		Time.timeScale = 0;
		Cursor.visible = true;
		pause.SetActive(true);
	}

	public void ClickResume() {
		mainUI.SetActive(true);
		Time.timeScale = 1;
		Cursor.visible = false;
		pause.SetActive(false);
	}

	public void ClickRestart() {
		ClickResume();
		load.Reload();
		load.ActivateScene();
	}

	public void ClickStartMenu() {
		load.StartLoading("MainMenu");
		load.ActivateScene();
	}

	public void AddScore(int newPoint) {
		MaxPoints += newPoint;
		score.text = MaxPoints.ToString("D4");
	}
}