﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

	public static AudioManager instance;

	#region variables

	[SerializeField]
	Effect doorOpen;
	[SerializeField]
	Effect doorClose;

	[SerializeField]
	Effect magnet;

	[SerializeField]
	Effect teleportOpen;
	[SerializeField]
	Effect teleportClose;
	[SerializeField]
	Effect teleport;

	[SerializeField]
	Effect grab;
	[SerializeField]
	Effect release;

	[SerializeField]
	Effect platformMove;
	[SerializeField]
	Effect platformEnd;

	[SerializeField]
	Effect receiverOn;
	[SerializeField]
	Effect receiverOff;

	[SerializeField]
	Effect ballFall;
	[SerializeField]
	Effect steps;
	[SerializeField]
	Effect backgroundMusic;

	[SerializeField]
	Effect item;

	[SerializeField]
	AudioMixer audioMixer;


	#endregion

	[System.Serializable]
	public struct Effect
	{
		public AudioClip clip;
		public AudioMixerGroup mixerGroup;
	}

	void Awake() {
		if (instance != null) {
			Destroy(gameObject);
		}
		else {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

    private void Start() {
		PlayEffect(transform, "backgroundMusic",1,0,0,true,true);
	}

    public AudioSource PlayEffect(Transform t, string effectName, float volume = 1, float pitch = 0.0f, float latency = 0.0f, bool overrideAudio = true, bool loop = false) {
		AudioSource audioSource = null;

		Effect effect = (Effect)this.GetType().GetField(effectName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(this);

		if (effect.clip != null) {
			audioSource = SelectAudioSource(t, overrideAudio, effect.clip);
			if (audioSource == null)
				audioSource = t.gameObject.AddComponent<AudioSource>();
			audioSource.clip = effect.clip;
			audioSource.outputAudioMixerGroup = effect.mixerGroup;

			if (latency > 0)
				audioSource.PlayDelayed(latency);
			else
				audioSource.Play();

			audioSource.pitch = 1f + pitch;
			audioSource.volume = volume;
			audioSource.loop = loop;
			//StartCoroutine("DestroyAudioAfterPlay", audioSource);
		}
		return audioSource;
	}

	public void CancelEffect(Transform t, string effectName) {
		Effect effect = (Effect)this.GetType().GetField(effectName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(this);

		if (effect.clip != null) {
			AudioSource[] sources = t.GetComponents<AudioSource>();

			foreach (AudioSource audioSource in sources) {
				if (audioSource.clip == effect.clip) {
					audioSource.Stop();
					print(audioSource.isPlaying);
					return;
				}
			}
		}
	}

	AudioSource SelectAudioSource(Transform t, bool overrideAudio, AudioClip clip) {
		AudioSource[] audioSources = t.GetComponents<AudioSource>();

		foreach (AudioSource audio in audioSources) {

			if (!audio.isPlaying) {
				return audio;
			}
			else if (overrideAudio && audio.clip == clip)
				return audio;
		}
		return null;
	}

	IEnumerator DestroyAudioAfterPlay(AudioSource audio) {
		yield return new WaitForSeconds(1);
		if (audio != null) {
			yield return new WaitUntil(() => !audio.isPlaying);
			Destroy(audio);
		}
	}

	public void SetMusicVolume(float soundLevel) {
		soundLevel = Mathf.Log10(soundLevel) * 20;
		audioMixer.SetFloat("MusicVolume", soundLevel);
	}

	public void SetEffectVolume(float soundLevel) {
		soundLevel = Mathf.Log10(soundLevel) * 20;
		audioMixer.SetFloat("EffectVolume", soundLevel);
	}

	public void SetSoundMasterVolume(float soundLevel) {
		soundLevel = Mathf.Log10(soundLevel) * 20;
		audioMixer.SetFloat("MasterVolume", soundLevel);
	}
}
