﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;

	[SerializeField]
	private bool hideCursor;

    [Header("Pause Menu")]
    public GameObject pauseMenu;
    private bool isPaused;

    [Header("End Game")]
    [SerializeField]
    GameObject defeatMenu;
    [SerializeField]
    GameObject victoryMenu;
    [SerializeField]
    bool isGameFinished;

    /*
    [Header("Music")]
    [SerializeField]
    AudioClip gameOverMusic;
    [SerializeField]
    AudioClip victoryMusic;
    AudioSource audios; */

    public bool IsPaused { get => isPaused; set => isPaused = value; }

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start () {
		if(hideCursor)
			Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel") && !isGameFinished)
        {
            if (IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
        IsPaused = true;
        pauseMenu.SetActive(IsPaused);
    }

    public void Resume()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        IsPaused = false;
        pauseMenu.SetActive(IsPaused);
    }

    public void Defeat()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
        isGameFinished = true;
        /* audios.clip = gameOverMusic;
        audios.loop = false;
        audios.Play(); */
        defeatMenu.SetActive(true);
    }

    public void Victory()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
        isGameFinished = true;
        /* audios.clip = victoryMusic;
        audios.loop = false;
        audios.Play(); */
        victoryMenu.SetActive(true);
    }
}
