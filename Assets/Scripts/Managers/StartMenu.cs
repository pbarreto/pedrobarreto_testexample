﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class StartMenu : MonoBehaviour
{
    LoadScene load;
    string lastLevel;
    [SerializeField]
    TextMeshProUGUI currentLevel;
    [SerializeField]
    GameObject continueButton;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        load = LoadScene.instance;
        lastLevel = CurrentLevelUpdater.instance.LoadCurrentLevel();
        if (lastLevel == string.Empty || lastLevel == "Level1")
            continueButton.SetActive(false);

        currentLevel.text = lastLevel;
    }

    public void ClickPlay() {
        load.StartLoading("Level1");
        load.ActivateScene();
    }
    public void ClickContinue() {
        load.StartLoading(lastLevel);
        load.ActivateScene();
    }

    public void Quit() {
        Application.Quit();
    }

}
