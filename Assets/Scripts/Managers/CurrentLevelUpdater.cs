﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CurrentLevelUpdater : MonoBehaviour
{
    public static CurrentLevelUpdater instance;

    void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        string name = SceneManager.GetActiveScene().name;
        if (name != "MainMenu" && name != "EndMenu")
            SaveCurrentLevel(SceneManager.GetActiveScene().name);
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void SaveCurrentLevel(string level) {
        PlayerPrefs.SetString("Level", level);
    }

    public string LoadCurrentLevel() {
        return PlayerPrefs.GetString("Level");
    }
}
