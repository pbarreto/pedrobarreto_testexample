﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
	Transform cam;
	[SerializeField]
	float interactRange = 5;
	[SerializeField]
	Transform hand;
	bool grabbing = false;
	IGrabable grab;

    public Transform Cam { get => cam; set => cam = value; }
    public float InteractRange { get => interactRange; set => interactRange = value; }
    public bool Grabbing { get => grabbing; set => grabbing = value; }

    private void Start() {
		Cam = Camera.main.transform;
	}

    void FixedUpdate() {
		if (Input.GetKeyDown(KeyCode.E)) {

            if (Grabbing) {
				Grabbing = false;
				if(grab != null)
					grab.Release();
			}
            else if(!Grabbing) {
				Interact();
			}
        }
	}

	void Interact() {

		RaycastHit hit;
		if (Physics.Raycast(Cam.position, Cam.forward, out hit, InteractRange)) {

			IInteractable interaction = hit.transform.GetComponent<IInteractable>();

			if (interaction != null)
				interaction.Interact();
            else {
				grab = hit.transform.GetComponent<IGrabable>();

				if (grab != null) {
					Grabbing = true;
					grab.Grab(hand);
				}
			}
		}
	}
}
