﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionUI : MonoBehaviour
{
    UIManager ui;
    Interaction interaction;

    void Start() {
        interaction = GetComponent<Interaction>();
        ui = UIManager.instance;
        InvokeRepeating("ShowGrab", 0, 0.1f);
    }

    void ShowGrab() {
        if (!interaction.Grabbing) {
            RaycastHit hit;
            if (Physics.Raycast(interaction.Cam.position, interaction.Cam.forward, out hit, interaction.InteractRange)) {

                IGrabable interaction = hit.transform.GetComponent<IGrabable>();

                if (interaction != null) {
                    ui.ShowGrab();
                    return;
                }
            }
        }
        else {
            ui.ShowRelease();
            return;
        }

        ui.HideGrab();
    }
}
