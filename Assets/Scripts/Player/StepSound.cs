﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(Rigidbody))]
public class StepSound : MonoBehaviour
{
    PlayerController player;
    Rigidbody rig;
    AudioManager audioManager;
    Timer time;
    [SerializeField]
    float soundSpeed = 0.35f;

    void Start()
    {
        player = GetComponent<PlayerController>();   
        rig = GetComponent<Rigidbody>();
        audioManager = AudioManager.instance;
        time = new Timer();
        time.BeginTimer(soundSpeed);
    }

    void Update()
    {
        if (player.IsGrounded && rig.velocity.magnitude > 1 && time.IsTime()) {
            time.BeginTimer(soundSpeed);
            float randomVolume = Random.Range(0.5f,8.0f);
            float randomPitch = Random.Range(0.5f,1.1f);
            audioManager.PlayEffect(transform,"steps",randomVolume,randomPitch,0,false);
        }
    }
}
