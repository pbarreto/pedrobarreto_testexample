﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
    #region variables
    public static PlayerController instance; 

	[SerializeField]
	private float moveSpeed = 10f;
    [SerializeField]
    private int numberOfJumps = 1;
    private int jumpCounter;
    private Rigidbody rigidBody;
    [SerializeField]
    private float rotationSpeed = 20.0f;
    [SerializeField]
    private float jumpHeight = 2f;
    [SerializeField]
    private float groundDistance = 0.2f;
    [SerializeField]
    private LayerMask groundMask;

    private bool isGrounded;
    private Transform groundChecker;

    private Camera cameraFollowing;
    private InputHandler input;

    public bool IsGrounded { get => isGrounded; }
    #endregion

    void Awake()
    {
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
		}
	}

    void Start() {
        rigidBody = GetComponent<Rigidbody>();
        groundChecker = transform.Find("GroundChecker");
        if (groundChecker == null)
            Debug.LogError("Please insert a ground checker in " + gameObject.name +" object.");
        input = InputHandler.instance;
        cameraFollowing = Camera.main;
    }

    private void Update() {
        Move();
        Jump();
    }

    void FixedUpdate() {
        isGrounded = Physics.CheckSphere(groundChecker.position, groundDistance, groundMask, QueryTriggerInteraction.Ignore);
    }

    void Move() {
        Vector3 dir = InputDirection();
        rigidBody.velocity = new Vector3(dir.x,0,dir.z) * moveSpeed + new Vector3(0, rigidBody.velocity.y, 0);
    }

    void Rotate(Vector3 direction) {
        Vector3 targetdirection = direction;

        targetdirection.y = 0;
        if (targetdirection == Vector3.zero)
            targetdirection = transform.forward;

        Quaternion tr = Quaternion.LookRotation(targetdirection);
        Quaternion targetRotation = Quaternion.Slerp(transform.rotation, tr, Time.fixedDeltaTime * rotationSpeed);
        rigidBody.MoveRotation(targetRotation);
    }

    void Jump() {
        if (input.ClickJumpButton) {
            if (IsGrounded) {
                ResetJumps();
            }

            if (jumpCounter < numberOfJumps) {
                jumpCounter++;
                rigidBody.velocity = Vector3.zero;
                rigidBody.angularVelocity = Vector3.zero;
                rigidBody.AddForce(Vector3.up * Mathf.Sqrt(-jumpHeight * Physics.gravity.y / jumpCounter), ForceMode.VelocityChange);
            }
        }
    }

    #region Aux Methods
    void ResetJumps() {
        jumpCounter = 0;
    }

    Vector3 InputDirection() {
        float horizontal = input.HorizontalAxis;
        float vertical = input.VerticalAxis;
        Vector3 moveVector = transform.right * horizontal + transform.forward * vertical;
        return moveVector.normalized;
    }

    #endregion
}
