﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonCamera : MonoBehaviour {

	private Vector2 mouseLook;
	private Vector2 smoothVelocity;
	[SerializeField]
	private float sensitivity = 5f;
	[SerializeField]
	private float smoothing = 2f;

	[SerializeField]
	Transform target;

	void Start () {
		Cursor.visible = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector2 md = new Vector2 (Input.GetAxisRaw("Mouse X"),Input.GetAxisRaw("Mouse Y") * sensitivity * smoothing);
		smoothVelocity.x = Mathf.Lerp (smoothVelocity.x, md.x, 1f / smoothing);
		smoothVelocity.y = Mathf.Lerp (smoothVelocity.y, md.y, 1f / smoothing);

		mouseLook += smoothVelocity;
		mouseLook.y = Mathf.Clamp (mouseLook.y, -90, 90);
		transform.localRotation = Quaternion.AngleAxis (-mouseLook.y,Vector3.right);
		target.localRotation = Quaternion.AngleAxis (mouseLook.x,target.up);
	}
}
