﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGrab : MonoBehaviour, IGrabable{

    Collider col;
    Rigidbody rig;
    AudioManager audio;

    private void Start() {
        col = GetComponent<Collider>();
        rig = GetComponent<Rigidbody>();
        audio = AudioManager.instance;
        if (audio == null)
            Debug.LogError("Please assign an AudioManager in the Hiearchy");

    }

    #region IGrabable implementation

    public void Grab(Transform hand) {
        //col.isTrigger = true; // col.enabled = false cant be used with Receivers use col.isTrigger = true instead
        rig.useGravity = false;
        rig.detectCollisions = false;
        transform.SetParent(hand);
        Quaternion rotation = transform.localRotation;
        rotation.eulerAngles = Vector3.zero;
        transform.localRotation = rotation;
        transform.localPosition = Vector3.zero;
        rig.constraints = RigidbodyConstraints.FreezeAll;
        audio.PlayEffect(transform,"grab");
    }

    public void Release() {
        //col.isTrigger = false; // col.enabled = true cant be used with Receivers  use col.isTrigger = true instead
        rig.detectCollisions = true;
        rig.constraints = RigidbodyConstraints.None;
        rig.useGravity = true;
        transform.SetParent(null);
        audio.PlayEffect(transform,"release");
    }

    public void OnCollisionEnter(Collision collision) {
        audio.PlayEffect(transform, "ballFall",0.2f,0.1f);
    }

    #endregion
}
