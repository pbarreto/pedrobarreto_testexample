﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Receiver : ObjectLogic
{
    public static event EventHandler<Receiver,bool> Change;

    AudioManager audio;

    void Start() {
        audio = AudioManager.instance;
    }

    private void OnTriggerEnter(Collider other) {
        IGrabable obj = other.GetComponent<IGrabable>();
        if(obj != null) {
            Activate();
            audio.PlayEffect(transform,"receiverOn",0.1f,0.2f);
            UpdateReceiver(Activated);
        }
    }

    private void OnTriggerExit(Collider other) {
        IGrabable obj = other.GetComponent<IGrabable>();
        if (obj != null) {
            Deactivate();
            audio.PlayEffect(transform, "receiverOff", 0.1f, 0.2f);
            UpdateReceiver(Activated);
        }
    }

    public void UpdateReceiver(bool activated) {
        if (Change != null)
            Change(this,activated);
    }
}
