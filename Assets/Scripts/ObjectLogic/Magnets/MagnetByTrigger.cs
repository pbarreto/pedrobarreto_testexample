﻿using UnityEngine;

public class MagnetByTrigger : Magnet {

	[SerializeField]
	bool ignoreGravity = true;

	void OnTriggerEnter(Collider col){
		if (col.GetComponent<Rigidbody>() != null && !objectsInsideTheField.Exists(x=> x == col.transform)){
			objectsInsideTheField.Add(col.transform);
			if(ignoreGravity)
				col.GetComponent<Rigidbody>().useGravity = false;
		}
	}

	void OnTriggerExit(Collider col){
		if (col.GetComponent<Rigidbody>() != null){
			objectsInsideTheField.Remove(col.transform);
			if(ignoreGravity)
				col.GetComponent<Rigidbody>().useGravity = true;
		}
	}
}
