﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Magnet : ObjectLogic {

	public float magneticForce;
	protected List<Transform> objectsInsideTheField;
    [SerializeField]
    bool ignoreMass;

    Collider col;
    MeshRenderer render;

    AudioManager audio;

    protected void Start () {
       // col = GetComponent<Collider>
		objectsInsideTheField = new List <Transform>();
        render = GetComponent<MeshRenderer>();
        col = GetComponent<Collider>();
        Deactivate();
        audio = AudioManager.instance;
    }

    public override void Deactivate() {
        base.Deactivate();
        Show(false);
    }

    public override void Activate() {
        base.Activate();
        audio.PlayEffect(transform,"magnet", 1, 0, 0, false,true);
        Show(true);
    }

    protected void Update () {
        if(Activated)
		    AttractAll ();
	}

    void Attract(Transform body)
    {
        if (body != null) {
            Vector3 gravityUp = transform.up;//(body.position - transform.position).normalized;

            if(ignoreMass)
                body.GetComponent<Rigidbody>().AddForce(gravityUp * magneticForce,ForceMode.VelocityChange);
            else body.GetComponent<Rigidbody>().velocity = gravityUp * magneticForce;
        }
    }

    void AttractAll(){
		foreach (Transform t in objectsInsideTheField){
            Attract(t);
        }
    }

    void Show(bool value) {
        col.enabled = value;
        render.enabled = value;
    }
}
