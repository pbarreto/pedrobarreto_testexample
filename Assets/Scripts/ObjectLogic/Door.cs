﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : ObjectLogic {
	Vector3 startPosition;
	Vector3 endPosition;
	[SerializeField]
	float speed = 1;

	IEnumerator playCoroutine;

	AudioManager audio;

    private void Start() {
		startPosition = transform.position;
		endPosition = startPosition + Vector3.down * 5;
		audio = AudioManager.instance;
		if (audio == null)
			Debug.LogError("Please assign an AudioManager in the Hiearchy");
	}

    new public void Play (){
		if(playCoroutine!= null)
			StopCoroutine(playCoroutine);
		base.Play();
		playCoroutine = Move();
		StartCoroutine(playCoroutine);
	}

	public override void Activate() {
		base.Activate();
		if (playCoroutine != null)
			StopCoroutine(playCoroutine);
		playCoroutine = Move();
		StartCoroutine(playCoroutine);
		audio.PlayEffect(transform,"doorOpen");
	}

	public override void Deactivate() {
		base.Deactivate();
		if (playCoroutine != null)
			StopCoroutine(playCoroutine);
		playCoroutine = Move();
		StartCoroutine(playCoroutine);
		audio.PlayEffect(transform,"doorClose");
	}


	IEnumerator Move() {
		Vector3 targetPosition = startPosition;

		if (Activated) {
			targetPosition = endPosition;
		}
		else if (!Activated) {
			targetPosition = startPosition;
		}

		while (Vector3.Distance(targetPosition, transform.position) > 0.01f) {
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
			yield return null;
		}
	}
}
