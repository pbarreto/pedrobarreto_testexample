﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : ObjectLogic
{
    public List<Vector3> pathNodes;
    public float moveSpeed;
    public float waitTime;
    float step;
    static Vector3 currentPositionHolder;
    int currentNode;
    private Vector3 startPosition;
    int moveDirection = 1;
    Timer waitTimer;
    bool waiting;

    AudioManager audioManager;

     enum State
    {
        FORWARD,
        BACKWARD
    }

    enum Type
    {
        Cicle,
        PingPong
    }
    [SerializeField]
    Type type;
    State state;

    void Start() {
        waitTimer = new Timer();
        if (pathNodes == null)
            pathNodes = new List<Vector3>();
        transform.position = pathNodes[currentNode];
        UpdateCurrentNode();
        audioManager = AudioManager.instance;
    }

    void UpdateCurrentNode() {
        currentPositionHolder = pathNodes[currentNode];
    }

    public void AddCurrentPosition() {
        pathNodes.Add(transform.position);
    }

    public void RemoveLastPosition() {
        pathNodes.Remove(transform.position);
    }


    void Update() {
        if(Activated)
            Behaviour();
    }

    void Behaviour() {
        step = Time.deltaTime * moveSpeed;

        if (transform.position != currentPositionHolder) {
            transform.position = Vector3.MoveTowards(transform.position, currentPositionHolder, step);
        }
        else {
            if (waiting == false) {
                waiting = true;
                waitTimer.BeginTimer(waitTime);
                audioManager.CancelEffect(transform,"platformMove");
                audioManager.PlayEffect(transform, "platformEnd", 0.3f, 0.5f);
            }
            else if(waitTimer.IsTime()){
                waiting = false;
                if ((moveDirection == 1 && currentNode < pathNodes.Count - 1) || (moveDirection == -1 && currentNode > 0)) {
                    currentNode += moveDirection;
                    UpdateCurrentNode();
                    audioManager.PlayEffect(transform,"platformEnd", 0.3f,0.5f);
                    audioManager.PlayEffect(transform,"platformMove", 0.3f,0.5f,0.5f);
                }
                else {
                    if (type == Type.PingPong) {
                        moveDirection = -moveDirection;
                    }
                    else if (type == Type.Cicle) {
                        currentNode = 0;
                        UpdateCurrentNode();
                    }
                }
            }
        }
    }
}