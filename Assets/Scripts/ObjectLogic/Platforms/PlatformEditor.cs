﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Platform))]
public class PlatformEditor : Editor
{
    public override void OnInspectorGUI() {
        Platform platform = (Platform)target;

        if (GUILayout.Button("Create New Position")) {
            platform.AddCurrentPosition();
        }

        if (GUILayout.Button("Remove Last Position")) {
            platform.RemoveLastPosition();
        }

        DrawDefaultInspector();
        EditorUtility.SetDirty(target);
    }
}

#endif