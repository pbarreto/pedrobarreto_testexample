﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiverUser : MonoBehaviour
{
    [SerializeField]
    List<Receiver> receiversList;
    IActionable[] actions;

    void Start() {

        actions = GetComponents<IActionable>();
        if (actions.Length < 0)
            Debug.LogError("Please insert a script that implements IActionable in " + gameObject.name);

        AddEventListeners();
    }

    void OnDisable() {
        RemoveListeners();
    }

    void AddEventListeners() {
        Receiver.Change += ChangeReceivers;
    }
    void RemoveListeners() {
        Receiver.Change -= ChangeReceivers;
    }

    void ChangeReceivers(Receiver r, bool activated) {
        if (VerifyReceiverExist(r)) {
            if (!activated) {
                DeactivateAllActions();
                return;
            }
            else if (VerifyReceiversAllActivated())
                ActivateAllActions();
        }
    }

    void ActivateAllActions() {
        for(int i=0; i< actions.Length; i++) {
            actions[i].Activate();
        }
    }

    void DeactivateAllActions() {
        for (int i = 0; i < actions.Length; i++) {
            actions[i].Deactivate();
        }
    }

    #region auxiliary methods
    bool VerifyReceiversAllActivated() {
        foreach (Receiver r in receiversList)
            if (!r.Activated)
                return false;
        return true;
    }

    bool VerifyReceiverExist(Receiver changedReceiver) {
        foreach (Receiver r in receiversList)
            if (r == changedReceiver)
                return true;
        return false;
    }

    #endregion
}
