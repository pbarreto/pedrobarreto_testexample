﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour, IInteractable
{

	[SerializeField]
	GameObject[] actionObjects;

	#region IInteractable implementation

	public void Interact (){
		foreach(GameObject obj in actionObjects){
			IActionable action = obj.GetComponent<IActionable> ();
			if (action != null)
				action.Play ();	
		}
	}

	#endregion
}
