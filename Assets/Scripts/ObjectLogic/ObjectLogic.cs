﻿using UnityEngine;

public abstract class ObjectLogic : MonoBehaviour, IActionable
{
    [SerializeField]
    private bool activated;

    public bool Activated { get => activated; set => activated = value; }

    public virtual void Play() {
        activated = !activated;
    }

    public virtual void Deactivate() {
        activated = false;
    }

    public virtual void Activate() {
        activated = true;
    }
}
