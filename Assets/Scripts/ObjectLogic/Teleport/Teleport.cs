﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : ObjectLogic
{
    [SerializeField]
    Transform target;
    bool teleported = false;
    [SerializeField]
    GameObject visual;
    Material material;
    float EmitionIntensity = 2;

    AudioManager audio;

    public bool Teleported { get => teleported; set => teleported = value; }

    private void Start() {
        audio = AudioManager.instance;
        material = visual.GetComponent<MeshRenderer>().material;
        if (Activated)
            ActivateWithNoSound();
        else DeactivateWithNoSound();
    }

    public override void Deactivate() {
        DeactivateWithNoSound();
        audio.PlayEffect(transform,"teleportClose");
    }

    public override void Activate() {
        ActivateWithNoSound();
        audio.PlayEffect(transform, "teleportOpen");
    }

    public void DeactivateWithNoSound() {
        base.Deactivate();
        material.SetColor("_EmissionColor", Color.black);
    }
    public void ActivateWithNoSound() {
        base.Activate();
        material.SetColor("_EmissionColor", Color.white * EmitionIntensity);
    }
    private void OnTriggerEnter(Collider other) {
        if (Activated) {
            if (!teleported) {
                teleported = true;
                TeleportObject(other.transform);
                audio.PlayEffect(transform,"teleport");
            }
            else {
                target.GetComponent<Teleport>().teleported = false;
                teleported = false;
            }
        }
    }
    private void OnTriggerExit(Collider other) {
        if (teleported) {
            target.GetComponent<Teleport>().teleported = true;
            teleported = false;
        }
    }

    private void TeleportObject( Transform obj) {
        obj.transform.position = target.position;
    }
}
