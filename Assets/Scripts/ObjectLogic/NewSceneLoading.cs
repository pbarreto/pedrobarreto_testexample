﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewSceneLoading : ObjectLogic
{
    [SerializeField]
    string sceneName = "Level1";

    public override void Activate() {
        LoadScene.instance.StartLoading(sceneName,true);
    }

    public override void Deactivate() {
        LoadScene.instance.DestroyLoading(sceneName);
    }

    public override void Play() {
        Activate();
    }
}
